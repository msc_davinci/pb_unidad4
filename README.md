# Unidad 4: Programación Básica

## **1. Introducción**

Ejercicio a resolver:

Escriba un método llamado distancia, para calcular la distancia entre dos puntos (x1, y1) y (x2, y2). Todos los números y valores de retorno deben ser de tipo double. Incorpore este método en una aplicación que permita al usuario introducir las coordenadas de los puntos. ¿Cuál sería el código fuente para este programa?

Requerimientos necesarios en tu computadora:
- Permisos de administrador para descargar e intalar software.
- Chocolatey package manager (sólo en Windows y es opcional).
- Homebrew (MAC y es opcional).
- Java JDK >1.8.
- Maven 3.5.
- IntelliJ, Eclipse Luna o cualquier otro IDE para java.

## **2. Herramientas**

Los lenguajes y lenguaje de programación utilizados en este projecto son:

1. Java como lenguaje de programación.
2. TestNG como herramienta de pruebas unitarias para ejecutar las pruebas.
3. Maven como herramienta para manejo de dependencias.
4. Git software de control de versiones.

## **3. Instalación de Herramientas**

Si tu equipo ya cuenta con Java 1.8 o mayor y Maven 3.4 o mayor, puedes omitir esta sección.

**Windows**
**Instalación de Chocolatey**
Visita la página https://chocolatey.org/install y sigue los pasos de instalación.

**Instalación de Java**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install jdk8 -y```.
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```. 
5. Listo para usar Java .

**Instalación de Maven**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install maven -y```.
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```.
5. Listo para usar Maven.

**Instalación de Git**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install git -y```
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```.
5. Listo para usar Git.

**MAC**
**Instalación de Homebrew**
Visita la página https://docs.brew.sh/Installation y sigue los pasos de instalación.

**Instalación de Java**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install java
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Java.

**Instalación de Maven**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install maven
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Maven.

**Instalación de Git**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install git
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Git.

## **3. Estructura del Proyecto**

Dentro de src >main >java >org >davinci >unidaddos; encontrarás la clase ```Factura``` donde se encuentran todos los atributos y métodos (getters y setter).

En el folder src >test >java >org >davinci >unidaddos, se encuentra la clase ```PruebaFactura```para ejecutar pruebas.

**Directorio**

    ├── src                                              # Archivos fuente
    │   ├── main                       
    |       ├── java
    |           ├── org
    |               ├── davinci
    |                   ├── distancia
    |                       ├── DistanciaPuntos.java     # Clase con atributos y métodos para Distancia 
    |   ├── test                                     
    |       ├── java
    |           ├── org
    |               ├── davinci
    |                   ├── distancia
    |                       ├── PruebaDistancia.java      # Clases para pruebas  
    ├── .gitignore                                        # Ignorar archivos              
    ├── pom.xml                                           # Archivo con dependencias
    └── README.md                                    

## **4. Ejecución del Proyecto y Resultados**

1. Abre una terminal o linea de comandos.
2. Clona el repositorio copiando, pegando y ejecutando lo siguiente:  
```git clone https://gsanchezm@bitbucket.org/gsanchezm/pb_unidad4.git```.
3. Ejecuta la clase PruebaDistancia que se encuentra en src>test>java>org>udavinci>distancia
4. Ingresa los primeros puntos cómo se muestran en el ejemplo:
```4,8```  
5. Ingresa los primeros puntos cómo se muestran en el ejemplo:
```2,-6```
6. La aplicación mostrará el resultado de la distancia entre dichos puntos.

![image](https://user-images.githubusercontent.com/24705055/47612348-dd4a4c80-da46-11e8-9713-887fa6aae841.png)

## **5. Notas**

Puedes ver los siguientes videos para comparar las respuestas de los ejercicios con la del software:

[Distancia entre dos puntos en el Plano - Ejeicicio Resuelto (Matemática) - 2](https://www.youtube.com/watch?v=J5rGTpvVW8o)

[DISTANCIA ENTRE DOS PUNTOS EN EL PLANO CARTESIANO](https://www.youtube.com/watch?v=NYmRn4EAdmc)