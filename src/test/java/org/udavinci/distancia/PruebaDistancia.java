package org.udavinci.distancia;

import java.util.Scanner;

public class PruebaDistancia {
    private static Scanner entrada;
    private static DistanciaPuntos distancia;
    private static String[] puntos;

    public static void main(String[] args) {
        entrada = new Scanner(System.in);
        distancia = new DistanciaPuntos();

        System.out.println("Introduce los valores de x1 y y1 separados por una coma, sigue el ejemplo:\n4,8");
        puntos = entrada.next().split(",");
        distancia.setX1(Double.valueOf(puntos[0]));
        distancia.setY1(Double.valueOf(puntos[1]));

        System.out.println("Introduce los valores de x2 y y2 separados por una coma, sigue el ejemplo: \n2,-6");

        puntos = entrada.next().split(",");
        distancia.setX2(Double.valueOf(puntos[0]));
        distancia.setY2(Double.valueOf(puntos[1]));

        System.out.printf("Los puntos dados son P(%.2f,%.2f) Q(%.2f,%.2f)",
                distancia.getX1(), distancia.getY1(), distancia.getX2(), distancia.getY2());
        System.out.printf("\nLa distancia entre los puntos es: %.2f\n", distancia.getDistancia());

    }
}